from django.db import models
from django.conf import settings
from django.contrib.auth.models import User


class Profile(models.Model):
    class Meta:
        verbose_name = 'Главная'
        verbose_name_plural = 'Главная'
        ordering = ['-id']

    user_pro = models.OneToOneField(User, verbose_name='Пользователь', on_delete=models.CASCADE, blank=True, null=True)
    auth_code = models.CharField('Код для входа', max_length=6, blank=True, null=True)
    photo = models.ImageField(verbose_name='Фото', upload_to='Фото', blank=True, null=True)
    name = models.CharField(verbose_name='ФИО', max_length=128, blank=True, null=True)
    currencies = models.ForeignKey('Currency', verbose_name='Валюта', on_delete=models.CASCADE, blank=True, null=True)
    total_amount = models.FloatField(verbose_name='ИТОГО', default=0)
    incomes_amount = models.FloatField(verbose_name='Доходы', default=0)
    costs_amount = models.FloatField(verbose_name='Расходы', default=0)
    total_budget = models.PositiveIntegerField(verbose_name='Итого', default=0)
    date = models.DateTimeField('Дата регистрации', auto_now_add=True, blank=True, null=True)


    def __str__(self):
        return f'{str(self.name)}'


class Currency(models.Model):
    class Meta:
        verbose_name = 'Валюта'
        verbose_name_plural = 'Волюты'
        ordering = ['-id']

    currencies = models.CharField(max_length=4, verbose_name='Валюта', default='KGS')

    def __str__(self):
        return f'{str(self.currencies)} '


class Account(models.Model):
    class Meta:
        verbose_name = 'Счёт'
        verbose_name_plural = 'Счета'
        ordering = ['-id']

    optional = 'Текущий'
    savings = 'Сбережения'
    ACCOUNT_CHOICES = (
        (optional, 'Текущий'),
        (savings, 'Сбережения')
    )
    profile = models.ForeignKey(Profile, verbose_name='Пользователь', related_name='account', on_delete=models.CASCADE,
                                blank=True, null=True)
    title = models.CharField(max_length=64, verbose_name='Счёт', blank=True, null=True)
    currency = models.ForeignKey(Currency, verbose_name='Валюта', on_delete=models.SET_NULL, blank=True, null=True)
    balance = models.FloatField(verbose_name='Баланс', default=0)
    type = models.CharField(max_length=12, choices=ACCOUNT_CHOICES, default='optional', blank=True)

    def __str__(self):
        return f'{str(self.title)}'


class IncomeCategory(models.Model):
    class Meta:
        verbose_name = 'Категория дохода'
        verbose_name_plural = 'Категории дохода'

    inc_categories = models.CharField(max_length=64, verbose_name='Категория')

    def __str__(self):
        return f'{str(self.inc_categories)} '


class Income(models.Model):
    class Meta:
        verbose_name = 'Доход'
        verbose_name_plural = 'Доходы'
        ordering = ['-id']

    # profile = models.ForeignKey(Profile, verbose_name='Пользователь', on_delete=models.CASCADE, blank=True, null=True)
    i_categories = models.ForeignKey(IncomeCategory, verbose_name='Категория', on_delete=models.SET_NULL, blank=False,
                                     null=True)
    accounts = models.ForeignKey(Account, verbose_name='Счёт', on_delete=models.CASCADE, blank=True, null=True,
                                 related_name="acc_income")
    income_amount = models.FloatField('Общая Сумма', default=0)
    date = models.DateTimeField('Дата', auto_now_add=True, blank=True, null=True)
    my_comment = models.CharField(verbose_name='Комментарий', max_length=128, blank=True, null=True)

    def __str__(self):
        return f'{str(self.income_amount)} '


class CostCategory(models.Model):
    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    c_categories = models.CharField(max_length=64, verbose_name='Категория')

    def __str__(self):
        return f'{str(self.c_categories)} '


class Cost(models.Model):
    class Meta:
        verbose_name = 'Расход'
        verbose_name_plural = 'Расходы'
        ordering = ['-id']

    c_categories = models.ForeignKey(CostCategory, verbose_name='Категория', on_delete=models.SET_NULL, blank=False,
                                     null=True)
    accounts = models.ForeignKey(Account, verbose_name='Счёт', on_delete=models.CASCADE, blank=True, null=True,
                                 related_name="acc_cost")
    cost_amount = models.FloatField('Сумма', default=0)
    date = models.DateTimeField('Дата', auto_now_add=True, blank=True, null=True)
    my_comment = models.CharField(verbose_name='Комментарий', max_length=128, blank=True, null=True)

    def __str__(self):
        return f'{str(self.cost_amount)}'


class MyBudget(models.Model):
    class Meta:
        verbose_name = 'Бюджет'
        verbose_name_plural = 'Бюджеты'
        ordering = ['-id']

    day = 'Бюджет на день'
    week = 'Бюджет на неделю'
    month = 'Бюджет на месяц'
    year = 'Бюджет на год'
    PERIOD_CHOICES = (
        (day, 'Бюджет на день'),
        (week, 'Бюджет на неделю'),
        (month, 'Бюджет на месяц'),
        (year, 'Бюджет на год')
    )
    profile = models.ForeignKey(Profile, verbose_name='Пользователь', on_delete=models.CASCADE, blank=True, null=True)
    date = models.DateTimeField('Дата', auto_now_add=True, blank=True, null=True)
    title = models.CharField(verbose_name='Название Бюджета', max_length=128, blank=True, null=True)
    period_budget = models.CharField(verbose_name='Период', max_length=32, choices=PERIOD_CHOICES, default='week',
                                     blank=True)
    budget_category = models.ForeignKey(CostCategory, verbose_name='Категория', on_delete=models.SET_NULL, blank=False,
                                        null=True)
    cost_budget = models.PositiveIntegerField(verbose_name='Итого', default=0)
    my_comment = models.CharField(verbose_name='Комментарий', max_length=128, blank=True, null=True)

    def __str__(self):
        return f'{str(self.title)}'
