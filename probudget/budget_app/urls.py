from django.urls import path
from .views import ProfileView, AccountView, CurrencyView, IncomeView, IncomeCategoryView, CostView, CostCategoryView, \
    MyBudgetView, MyBudgetView, RegisterView, LoginView, CheckPhoneView


urlpatterns = [
    path('register/', RegisterView.as_view()),
    path('login/', LoginView.as_view()),
    path('check_phone/', CheckPhoneView.as_view()),
    path('profile/', ProfileView.as_view({'post': 'create'})),
    path('profile/<int:pk>/', ProfileView.as_view({'get': 'retrieve', 'delete': 'destroy', 'put': 'update'})),
    path('profile/currency/', CurrencyView.as_view({'get': 'list'})),

    path('account/', AccountView.as_view({'post': 'create'})),
    path('account/<int:pk>/', AccountView.as_view({'get': 'retrieve', 'delete': 'destroy', 'put': 'update'})),

    path('income/', IncomeView.as_view({'get': 'list', 'post': 'create'})),
    path('income/<int:pk>/', IncomeView.as_view({'get': 'retrieve', 'delete': 'destroy', 'put': 'update'})),
    path('income/i_category/', IncomeCategoryView.as_view({'get': 'list'})),

    path('cost/', CostView.as_view({'post': 'create'})),
    path('cost/<int:pk>/', CostView.as_view({'get': 'retrieve', 'delete': 'destroy', 'put': 'update'})),
    path('cost/c_category/', CostCategoryView.as_view({'get': 'list'})),

    path('my_budget/', MyBudgetView.as_view({'post': 'create'})),
    path('my_budget/<int:pk>/', MyBudgetView.as_view({'get': 'retrieve', 'delete': 'destroy', 'put': 'update'})),
    path('my_budget/c_category/', CostCategoryView.as_view({'get': 'list'})),
]