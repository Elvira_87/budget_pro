from django.contrib import admin
from .models import Profile, Account, Currency, IncomeCategory, Income, CostCategory, Cost
from modeltranslation.admin import TranslationAdmin, TabbedTranslationAdmin


class ProfileAdmin(TabbedTranslationAdmin):
    pass


class AccountAdmin(TabbedTranslationAdmin):
    pass


class IncomeCategoryAdmin(TabbedTranslationAdmin):
    pass


class IncomeAdmin(TabbedTranslationAdmin):
    pass


class CostCategoryAdmin(TabbedTranslationAdmin):
    pass


class CostAdmin(TabbedTranslationAdmin):
    pass


admin.site.register(Profile, ProfileAdmin)
admin.site.register(Account, AccountAdmin)
admin.site.register(Currency)
admin.site.register(IncomeCategory, IncomeCategoryAdmin)
admin.site.register(Income, IncomeAdmin)
admin.site.register(CostCategory, CostCategoryAdmin)
admin.site.register(Cost, CostAdmin)
