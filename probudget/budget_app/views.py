from datetime import datetime

import django_filters
from rest_framework import status, permissions
from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication
from rest_framework import filters
from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView
from rest_framework.pagination import PageNumberPagination
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User

from _collections import OrderedDict
from .models import Profile, Income, Currency, CostCategory, IncomeCategory, Account, Cost, MyBudget
from .permissions import IsProfileOwner, IsAccountOwner, IsIncomeOwner, IsCostOwner, IsMyBudgetOwner
from .serializations import ProfileSerializer, CurrencySerializer, AccountSerializer, IncomeCategorySerializer, \
    IncomeSerializer, CostCategorySerializer, CostSerializer, MyBudgetSerializer
from .utils import generate_code


class RegisterView(APIView):

    def post(self, request):
        username = request.data.get('username')
        if username:
            user = User.objects.create(username=username)
            Token.objects.create(user=user)
            code = generate_code()
            profile = Profile.objects.create(user_pro=user, auth_code=code)
            # TODO send sms
            return Response('registered', status=status.HTTP_201_CREATED)
        return Response('error', status=status.HTTP_400_BAD_REQUEST)


class LoginView(APIView):

    def post(self, request):
        username = request.data.get('username')
        code = request.data.get('code')
        if username and code:
            user = User.objects.get(username=username)
            profile = Profile.objects.get(user_pro=user)
            if code == profile.auth_code:
                new_code = generate_code()
                profile.auth_code = new_code
                profile.save()
                token = Token.objects.get(user=user)
                return Response(f'{token.key}', status=status.HTTP_200_OK)
            return Response('wrong code', status=status.HTTP_400_BAD_REQUEST)
        return Response('need full data', status=status.HTTP_400_BAD_REQUEST)


class CheckPhoneView(APIView):

    def post(self, request):
        username = request.data.get('username')
        if username:
            if User.objects.filter(username=username):
                user = User.objects.get(username=username)
                profile = Profile.objects.get(user_pro=user)
                code = profile.auth_code
                # TODO send sms
                return Response('We have this number in our database', status=status.HTTP_200_OK)
            return Response("We haven't got this number in our database", status=status.HTTP_400_BAD_REQUEST)
        return Response('You need to send your phone number', status=status.HTTP_400_BAD_REQUEST)



# PAGE NUMERATION
class Pagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 200

    def get_paginated_response(self, data):
        return Response(OrderedDict([
            ('count', self.page.paginator.count),
            ('page_count', self.page.paginator.num_pages),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('results', data),
        ]))


class ProfileView(ModelViewSet):
    authentication_classes = [TokenAuthentication, ]
    permission_classes = [IsProfileOwner, ]
    serializer_class = ProfileSerializer
    queryset = Profile.objects.all()
    lookup_field = 'pk'
    filter_backends = ([django_filters.rest_framework.DjangoFilterBackend, filters.SearchFilter])
    filter_fields = ('user_pro', 'name', 'date')
    search_fields = ('user_pro', 'name', 'date')

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['request_user'] = self.request.user
        return context

    # Sort the products py their qualification
    def get_queryset(self):
        queryset = Profile.objects.all()
        order_field = self.request.GET.get('order')
        filter_fields = {}

        if self.request.GET.get('name'):
            filter_fields['name'] = self.request.GET.get('name')

        if self.request.GET.get('total_amount'):
            filter_fields['total_amount'] = self.request.GET.get('total_amount')

        if self.request.GET.get('currency'):
            filter_fields['currency'] = self.request.GET.get('currency')

        if self.request.GET.get('income_amount'):
            filter_fields['income_amount'] = self.request.GET.get('income_amount')

        if self.request.GET.get('costs_amount'):
            filter_fields['costs_amount'] = self.request.GET.get('costs_amount')

        if self.request.GET.get('accounts'):
            filter_fields['accounts'] = self.request.GET.get('accounts')

        if order_field:
            queryset = queryset.order_by(order_field)

        if filter_fields:
            queryset = queryset.filter(**filter_fields)

        start_date = self.request.GET.get('start_date')
        end_date = self.request.GET.get('end_date')
        currency = self.request.GET.get('currency')
        accounts = self.request.GET.get('accounts')

        if start_date and end_date:
            start_date = datetime.strptime(start_date, '%d.%m.%Y')
            end_date = datetime.strptime(end_date, '%d.%m.%Y')
            queryset = queryset.filter(date__date__gte=start_date, date__date__lte=end_date)
        if currency:
            queryset = queryset.filter(currency=currency)
        if accounts:
            queryset = queryset.filter(accounts=accounts)
        return queryset

    def create(self, request, *args, **kwargs):
        serializer = ProfileSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def perform_destroy(self, instance):
        instance.delete()

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def perform_update(self, serializer):
        serializer.save()

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)


class CurrencyView(ModelViewSet):
    authentication_classes = [TokenAuthentication, ]
    serializer_class = CurrencySerializer
    queryset = Currency.objects.all()
    lookup_field = 'pk'
    pagination_class = Pagination

    def my_currency(self, request, *args, **kwargs):
        user_profile = Profile.objects.get(user_pro=request.user)
        instance = Currency.objects.filter(profile=user_profile)
        serializer = self.get_serializer(instance, many=True)
        return Response(serializer.data)

    def get_queryset(self):
        queryset = Currency.objects.all()
        return queryset


class AccountView(ModelViewSet):
    authentication_classes = [TokenAuthentication, ]
    permission_classes = [IsAccountOwner, ]
    serializer_class = AccountSerializer
    queryset = Account.objects.all()
    lookup_field = 'pk'
    pagination_class = Pagination

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['request_user'] = self.request.user
        return context

    def my_accounts(self, request, *args, **kwargs):
        user_profile = Profile.objects.get(user_pro=request.user)
        instance = Account.objects.filter(profile=user_profile)
        serializer = self.get_serializer(instance, many=True)
        return Response(serializer.data)

    def get_queryset(self):
        queryset = Account.objects.all()
        return queryset

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def perform_destroy(self, instance):
        instance.delete()

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def perform_update(self, serializer):
        serializer.save()

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)


class IncomeCategoryView(ModelViewSet):
    authentication_classes = [TokenAuthentication, ]
    serializer_class = IncomeCategorySerializer
    queryset = IncomeCategory.objects.all()
    lookup_field = 'pk'
    pagination_class = Pagination

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['request_user'] = self.request.user
        return context

    def get_queryset(self):
        queryset = IncomeCategory.objects.all()
        return queryset


class IncomeView(ModelViewSet):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsIncomeOwner, ]
    serializer_class = IncomeSerializer
    queryset = Income.objects.all()
    lookup_field = 'pk'
    pagination_class = Pagination
    filter_backends = ([django_filters.rest_framework.DjangoFilterBackend, filters.SearchFilter])
    filter_fields = ('accounts', 'i_categories')
    search_fields = ('accounts', 'i_categories', 'income_amount', 'date')

    # Sort the products py their qualification
    def get_queryset(self):
        queryset = Income.objects.all()
        order_field = self.request.GET.get('order')
        filter_fields = {}

        if self.request.GET.get('i_categories'):
            filter_fields['i_categories'] = self.request.GET.get('i_categories')

        if self.request.GET.get('accounts'):
            filter_fields['accounts'] = self.request.GET.get('accounts')

        if self.request.GET.get('income_amount'):
            filter_fields['income_amount'] = self.request.GET.get('income_amount')

        if self.request.GET.get('date'):
            filter_fields['date'] = self.request.GET.get('date')

        if order_field:
            queryset = queryset.order_by(order_field)

        if filter_fields:
            queryset = queryset.filter(**filter_fields)

        start_date = self.request.GET.get('start_date')
        end_date = self.request.GET.get('end_date')
        i_categories = self.request.GET.get('i_categories')
        accounts = self.request.GET.get('accounts')

        if start_date and end_date:
            start_date = datetime.strptime(start_date, '%d.%m.%Y')
            end_date = datetime.strptime(end_date, '%d.%m.%Y')
            queryset = queryset.filter(date__date__gte=start_date, date__date__lte=end_date)
        if i_categories:
            queryset = queryset.filter(i_categories=i_categories)
        if accounts:
            queryset = queryset.filter(accounts=accounts)
        return queryset

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        acc_inc = Account.objects.get(acc_income=instance)
        serializer = self.get_serializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        acc_inc.balance -= instance.income_amount
        acc_inc.balance += float(request.data['income_amount'])
        acc_inc.save()
        profile = Profile.objects.get(account=acc_inc)
        profile.incomes_amount -= instance.income_amount
        profile.incomes_amount += float(request.data['income_amount'])
        profile.total_amount -= instance.income_amount
        profile.total_amount += float(request.data['income_amount'])
        profile.save()
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        acc_inc = Account.objects.get(acc_income=instance)
        acc_inc.balance -= instance.income_amount
        acc_inc.save()
        profile = Profile.objects.get(account=acc_inc)
        profile.incomes_amount -= instance.income_amount
        profile.total_amount -= instance.income_amount
        profile.save()
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def perform_destroy(self, instance):
        instance.delete()


class CostCategoryView(ModelViewSet):
    authentication_classes = [TokenAuthentication, ]
    serializer_class = CostCategorySerializer
    queryset = CostCategory.objects.all()
    lookup_field = 'pk'
    pagination_class = Pagination

    def get_queryset(self):
        queryset = CostCategory.objects.all()
        return queryset


class CostView(ModelViewSet):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsCostOwner, ]
    serializer_class = CostSerializer
    queryset = Cost.objects.all()
    lookup_field = 'pk'
    pagination_class = Pagination
    filter_backends = ([django_filters.rest_framework.DjangoFilterBackend, filters.SearchFilter])
    filter_fields = ('accounts', 'c_categories')
    search_fields = ('c_categories', 'accounts', 'cost_amount', 'date')

    # Sort the products py their qualification
    def get_queryset(self):
        queryset = Cost.objects.all()
        order_field = self.request.GET.get('order')
        filter_fields = {}

        if self.request.GET.get('c_categories'):
            filter_fields['c_categories'] = self.request.GET.get('c_categories')

        if self.request.GET.get('accounts'):
            filter_fields['accounts'] = self.request.GET.get('accounts')

        if self.request.GET.get('cost_amount'):
            filter_fields['cost_amount'] = self.request.GET.get('cost_amount')

        if self.request.GET.get('date'):
            filter_fields['date'] = self.request.GET.get('date')

        if order_field:
            queryset = queryset.order_by(order_field)

        if filter_fields:
            queryset = queryset.filter(**filter_fields)

        start_date = self.request.GET.get('start_date')
        end_date = self.request.GET.get('end_date')
        c_category = self.request.GET.get('c_categories')
        accounts = self.request.GET.get('accounts')

        if start_date and end_date:
            start_date = datetime.strptime(start_date, '%d.%m.%Y')
            end_date = datetime.strptime(end_date, '%d.%m.%Y')
            queryset = queryset.filter(date__date__gte=start_date, date__date__lte=end_date)
        if accounts:
            queryset = queryset.filter(accounts=accounts)
        if c_category:
            queryset = queryset.filter(c_categories=c_category)
        return queryset

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        acc_cost = Account.objects.get(acc_cost=instance)
        serializer = self.get_serializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        acc_cost.balance += instance.cost_amount
        acc_cost.balance -= float(request.data['cost_amount'])
        acc_cost.save()
        profile_cost = Profile.objects.get(account=acc_cost)
        profile_cost.costs_amount -= instance.cost_amount
        profile_cost.costs_amount += float(request.data['cost_amount'])
        profile_cost.total_amount += instance.cost_amount
        profile_cost.total_amount -= float(request.data['cost_amount'])
        profile_cost.save()
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        acc_c = Account.objects.get(acc_cost=instance)
        acc_c.balance += instance.cost_amount
        acc_c.save()
        profile_cost = Profile.objects.get(account=acc_c)
        profile_cost.costs_amount -= instance.cost_amount
        profile_cost.total_amount += instance.cost_amount
        profile_cost.save()
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def perform_destroy(self, instance):
        instance.delete()


class MyBudgetView(ModelViewSet):
    authentication_classes = [TokenAuthentication, ]
    permission_classes = [IsMyBudgetOwner, ]
    serializer_class = MyBudgetSerializer
    queryset = MyBudget.objects.all()
    lookup_field = 'pk'
    filter_backends = ([django_filters.rest_framework.DjangoFilterBackend, filters.SearchFilter])
    filter_fields = ('title', 'date', 'budget_category')
    search_fields = ('date', 'title', 'period_budget', 'budget_category', 'cost_budget', 'my_comment')

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['request_user'] = self.request.user
        return context

    # Sort the products py their qualification
    def get_queryset(self):
        queryset = MyBudget.objects.all()
        order_field = self.request.GET.get('order')
        filter_fields = {}

        if self.request.GET.get('title'):
            filter_fields['title'] = self.request.GET.get('title')

        if self.request.GET.get('period_budget'):
            filter_fields['period_budget'] = self.request.GET.get('period_budget')

        if self.request.GET.get('budget_category'):
            filter_fields['budget_category'] = self.request.GET.get('budget_category')

        if self.request.GET.get('cost_budget'):
            filter_fields['cost_budget'] = self.request.GET.get('cost_budget')

        if self.request.GET.get('date'):
            filter_fields['date'] = self.request.GET.get('date')

        if self.request.GET.get('my_comment'):
            filter_fields['my_comment'] = self.request.GET.get('my_comment')

        if order_field:
            queryset = queryset.order_by(order_field)

        if filter_fields:
            queryset = queryset.filter(**filter_fields)

        start_date = self.request.GET.get('start_date')
        end_date = self.request.GET.get('end_date')
        category = self.request.GET.get('budget_category')

        if start_date and end_date:
            start_date = datetime.strptime(start_date, '%d.%m.%Y')
            end_date = datetime.strptime(end_date, '%d.%m.%Y')
            queryset = queryset.filter(date__date__gte=start_date, date__date__lte=end_date)
        if category:
            queryset = queryset.filter(budget_categoriy=category)
        return queryset

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        budget = Profile.objects.get(user_pro=request.user)
        budget.total_budget += int(request.data['cost_budget'])
        budget.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        budget = Profile.objects.get(user_pro=request.user)
        budget.total_budget -= int(request.data['cost_budget'])
        budget.save()
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def perform_destroy(self, instance):
        instance.delete()

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance=instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        budget = Profile.objects.get(user_pro=request.user)
        budget.total_budget -= instance.cost_amount
        budget.total_budget += int(request.data['cost_budget'])
        budget.save()
        self.perform_update(serializer)
        return Response(serializer.data)


    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)
