from rest_framework.permissions import BasePermission
from .models import Profile, Account, IncomeCategory, CostCategory, Income, Cost, MyBudget


class IsProfileOwner(BasePermission):
    def has_permission(self, request, view):
        if request.method == 'POST':
            return True
        else:
            user = request.user
            profile = Profile.objects.get(id=view.kwargs['pk'])
            return user == profile.user_pro


class IsAccountOwner(BasePermission):
    def has_permission(self, request, view):
        if request.method == 'POST':
            return True
        else:
            user = request.user
            account = Account.objects.get(id=view.kwargs['pk'])
            return user == account.profile.user_pro


class IsIncomeOwner(BasePermission):
    def has_permission(self, request, view):
        if request.method == 'POST':
            return True
        else:
            user = request.user
            income = Income.objects.get(id=view.kwargs['pk'])
            acc = Account.objects.get(acc_income=income)
            return user == acc.profile.user_pro


class IsCostOwner(BasePermission):
    def has_permission(self, request, view):
        if request.method == 'POST':
            return True
        else:
            user = request.user
            cost = Cost.objects.get(id=view.kwargs['pk'])
            acc = Account.objects.get(acc_cost=cost)
            return user == acc.profile.user_pro


class IsMyBudgetOwner(BasePermission):
    def has_permission(self, request, view):
        if request.method == 'POST':
            return True
        else:
            user = request.user
            budget = MyBudget.objects.get(id=view.kwargs['pk'])
            return user == budget.profile.user_pro
