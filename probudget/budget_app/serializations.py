from requests import Response
from rest_framework import serializers, status
from .models import Profile, Currency, Account, IncomeCategory, Income, CostCategory, Cost, MyBudget


class ProfileSerializer(serializers.ModelSerializer):
    user_pro = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = Profile
        fields = ('user_pro', 'name', 'total_amount', 'currencies', 'incomes_amount', 'costs_amount', 'account',
                  'total_budget', 'date', 'id')

    def create(self, validated_data):
        user_pro = Profile.objects.create(**validated_data)
        user_pro.user_pro = self.context['request_user']
        user_pro.save()
        print(self.context['request_user'])
        return user_pro


class CurrencySerializer(serializers.ModelSerializer):

    class Meta:
        model = Currency
        fields = ('currencies', 'id')

    def create(self, validated_data):
        currency = Currency.objects.create(**validated_data)
        user_profile = Profile.objects.get(user_pro=self.context['request_user'])
        currency.profile = user_profile
        currency.save()
        print(self.context['request_user'])
        return currency


class AccountSerializer(serializers.ModelSerializer):
    user_pro = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = Account
        fields = ('user_pro', 'title', 'currency', 'balance', 'type', 'id')

    def create(self, validated_data):
        account = Account.objects.create(**validated_data)
        user_profile = Profile.objects.get(user_pro=self.context['request_user'])
        account.profile = user_profile
        account.save()
        return account


class IncomeCategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = IncomeCategory
        fields = ('inc_categories', 'id')

    def create(self, validated_data):
        i_cat = IncomeCategory.objects.create(**validated_data)
        user_pro = Profile.objects.get(user_pro=self.context['request_user'])
        i_cat.profile = user_pro
        i_cat.save()
        return i_cat


class IncomeSerializer(serializers.ModelSerializer):
    date = serializers.DateTimeField(read_only=True, format='%d.%m.%Y %H:%M')
    account_name = serializers.SerializerMethodField()

    class Meta:
        model = Income
        fields = ('accounts', 'account_name', 'i_categories', 'income_amount', 'date', 'my_comment', 'id')

    def create(self, validated_data):
        income = Income.objects.create(**validated_data)
        acc = Account.objects.get(acc_income=income)
        acc.balance += income.income_amount
        acc.save()
        profile = Profile.objects.get(account=acc)
        profile.incomes_amount += income.income_amount
        profile.total_amount += income.income_amount
        profile.save()
        return income

    def get_account_name(self, obj):
        return obj.accounts.title


class CostCategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = CostCategory
        fields = ('c_categories', 'id')

    def create(self, validated_data):
        c_cat = CostCategory.objects.create(**validated_data)
        user_pro = Profile.objects.get(user_pro=self.context['request_user'])
        c_cat.profile = user_pro
        c_cat.save()
        return c_cat


class CostSerializer(serializers.ModelSerializer):
    date = serializers.DateTimeField(read_only=True, format='%d.%m.%Y %H:%M')
    account_name = serializers.SerializerMethodField()

    class Meta:
        model = Cost
        fields = ('accounts', 'account_name', 'c_categories', 'cost_amount', 'date', 'my_comment', 'id')

    def create(self, validated_data):
        cost = Cost.objects.create(**validated_data)
        acc = Account.objects.get(acc_cost=cost)
        acc.balance -= cost.cost_amount
        acc.save()
        profile = Profile.objects.get(account=acc)
        profile.costs_amount += cost.cost_amount
        profile.total_amount -= cost.cost_amount
        profile.save()
        return cost

    def get_account_name(self, obj):
        return obj.accounts.title


class MyBudgetSerializer(serializers.ModelSerializer):
    user_pro = serializers.PrimaryKeyRelatedField(read_only=True)
    date = serializers.DateTimeField(read_only=True, format='%d.%m.%Y %H:%M')
    budget_cgry= serializers.SerializerMethodField()

    class Meta:
        model = MyBudget
        fields = ('user_pro', 'period_budget',  'budget_category', 'budget_cgry', 'cost_budget', 'my_comment', 'date', 'id')

    def create(self, validated_data):
        budget = MyBudget.objects.create(**validated_data)
        user_profile = Profile.objects.get(user_pro=self.context['request_user'])
        budget.profile = user_profile
        budget.save()
        return budget

    def get_budget_cgry(self, obj):
        return obj.budget_category.c_categories
