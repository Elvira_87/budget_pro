from modeltranslation.translator import register, TranslationOptions
from .models import Profile, Currency, Account, IncomeCategory, Income, CostCategory, Cost


@register(Profile)
class ProfileTranslationOptions(TranslationOptions):
    fields = ('name', )


@register(Account)
class AccountTranslationOptions(TranslationOptions):
    fields = ('title', 'type')


@register(IncomeCategory)
class IncomeCategoryTranslationOptions(TranslationOptions):
    fields = ('inc_categories', )


@register(Income)
class IncomeTranslationOptions(TranslationOptions):
    fields = ('my_comment', )


@register(CostCategory)
class CostCategoryTranslationOptions(TranslationOptions):
    fields = ('c_categories', )


@register(Cost)
class IncomeCategoryTranslationOptions(TranslationOptions):
    fields = ('my_comment',)
